﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : BaseShip
{
    public GameObject Bullet;
    public GameObject BulletEmitter;
    public int bulletSpeed;
    // Start is called before the first frame update
    void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        getAxis();
        
        if (Input.GetKeyDown("space"))
        {
            ObjectPooler.Instance.SpawnFromPool("bullet1",BulletEmitter.transform.position, Quaternion.identity);

        }
        
        base.Update();
    }

    private void getAxis()
    {
        axis.x = Input.GetAxis("Horizontal");
        axis.y = Input.GetAxis("Vertical");
        
    }

    
}

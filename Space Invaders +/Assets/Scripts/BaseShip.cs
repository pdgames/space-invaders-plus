﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseShip : MonoBehaviour
{
    public int speed;
    private Transform myTransform;
    protected Vector2 axis;
    // Start is called before the first frame update
    protected virtual void Start()
    {
        myTransform = transform;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        myTransform.Translate(axis * speed * Time.deltaTime);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BulletBehaviour : MonoBehaviour, IPooledbullet
{

    public int speed;
    private Transform myTransform;
    protected Vector2 movement = new Vector2(0,1);
    public float lifeTime;
    // Start is called before the first frame update
    protected virtual void Start()
    {
        myTransform = transform;

        
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        myTransform.Translate(movement * speed * Time.deltaTime);
    }

    public void DestroyBullet()
    {
        this.gameObject.SetActive(false);
    }
    public void OnObjectSpawn()
    {
        Invoke("DestroyBullet", lifeTime);
    }

}
